﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] DailyRewards dailyRewards;
    [SerializeField] GameObject dailyRewardsObj;
    [Space(5)]
    [SerializeField] AudioManager audioManager;
    [SerializeField] GameObject setings;
    [Space(5)]
    [SerializeField] GameObject tutorial;
    public void OpenDailyRewards()
    {
        dailyRewardsObj.SetActive(true);
        dailyRewards.StartCoroutine();
    }
    public void CloseDailyRewards()
    {
        dailyRewardsObj.SetActive(false);
    }
    public void OpenSetings()
    {
        audioManager.openSettings();
        setings.SetActive(true);
    }
    public void CloseSetings()
    {
        setings.SetActive(false);
    } 
    public void OpenTutorial()
    {
        tutorial.SetActive(true);
    }
    public void CloseTutorial()
    {
        tutorial.SetActive(false);
    }
    public void QuitBtn()
    {
        SaveManager.Instance.SaveGame();
        Application.Quit();
    }
}
