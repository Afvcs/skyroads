﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private Button buttonPause;
    [SerializeField] private PlayerMovement playerMovement;

    public void Pause()
    {
        gameObject.SetActive(true);
        gameManager.canPlay = false;
        gameManager.StopAllCoroutines();
        playerMovement.Pause();
        buttonPause.gameObject.SetActive(false);

    }
    public void Resume()
    {
           
        gameObject.SetActive(false);
        gameManager.canPlay = true;
        gameManager.AllStartCoroutine();
        playerMovement.UnPause();
        buttonPause.gameObject.SetActive(true);

    }
    public void MenuBtn()
    {
        gameObject.SetActive(false);
        mainMenu.SetActive(true);
    }
}
