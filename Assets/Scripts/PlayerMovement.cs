﻿using System.Collections;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float boostSpeed = 5;
    [SerializeField] private GameManager gameManager;
    [Space(5)]
    [SerializeField] private UnityAdsHelper unityAdsHelper;
    [SerializeField] private GameObject explosionEffect;
    private int count_add = 0;
    Rigidbody rb;
    public Vector3 mousePos;
    public Vector3 mousePosClick;


    private int laneNumber = 1,
                lanesCount = 2;

    public float firstLanePos,
                 laneDistance,
                 SideSpeed;
    private Vector3 startPosition;
    private Vector3 rbVelocity;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        transform.position = new Vector3(2.6f, 0.7f, 0);
        startPosition = transform.position;
        Debug.Log(transform.position);
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        { mousePosClick = Input.mousePosition; }
        if (Input.GetMouseButtonUp(0))
        {
            mousePosClick = new Vector3(0, 0, 0);
            gameManager.boostOff();
        }


        if (gameManager.canPlay)
        {
            CheckInput();

            Vector3 newPos = transform.position;
            newPos.z = Mathf.Lerp(newPos.z, firstLanePos + (laneNumber * laneDistance), Time.deltaTime * SideSpeed);
            transform.position = newPos;
        }

    }
    void CheckInput()
    {
        if (gameManager.canPlay)
        {
            if (mousePosClick == new Vector3(0, 0, 0))
            {
                laneNumber = 1;
                transform.rotation = Quaternion.Euler(0, 90, 0);
            }
            else if (mousePosClick.x < 640)
            {
                laneNumber = 0;
                transform.rotation = Quaternion.Euler(0, 90, 20);

            }
            else if (mousePosClick.x > 1280)
            {
                laneNumber = 2;
                transform.rotation = Quaternion.Euler(0, 90, -20);
            }
            else
            {
                gameManager.boostOn();
            }
        }
    }
    public void Pause()
    {
        rbVelocity = rb.velocity;
    }
    public void UnPause()
    {
        rb.velocity = rbVelocity;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("comet"))
        {
            collision.gameObject.SetActive(false);
            AudioManager.Instance.PlayBoomEffect();
            Instantiate(explosionEffect, transform.position, Quaternion.identity);
            StartCoroutine(Death());
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coins"))
        {
            AudioManager.Instance.PlayCoinEffect();
            System.Console.WriteLine("+1");
            gameManager.AddCoins(1);
            Destroy(other.gameObject);
        }
    }

    IEnumerator Death()
    {
        gameManager.canPlay = false;
        yield return new WaitForSeconds(0.3f);
        if (count_add == 3)
        {
            unityAdsHelper.DemoRewardedVideoAd();
            count_add = 0;
        }
        else
        {
            count_add++;
        }
        gameManager.ShowResult();

    }
    public void ResetPosition()
    {
        transform.position = startPosition;
        laneNumber = 1;
    }

}
