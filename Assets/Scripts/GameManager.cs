﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject resultObj;
    public GameObject congratulations;
    [SerializeField] private PlayerMovement playerMovement;
    [SerializeField] private RoadSpawner roadSpawner;
    [SerializeField] public float moveSpead;
    [SerializeField] private Text pointText;
    [SerializeField] private Text countCometText;
    [SerializeField] private Text bestPointText;
    [SerializeField] private Text coinsText;
    [SerializeField] private Text timeText;
    [SerializeField] private CountComet countComet;
    [SerializeField] private SmoothFollow camera;
    [SerializeField] private PauseController pauseController;
    [SerializeField] private GameObject mainMenu;

    public List<Skin> Skins;


    public int point;
    public int bestPoint;
    public int coins;
    private bool boostEnable;
    public float timeInPlay;

    public bool canPlay;
    public void ShowResult()
    {
        if (bestPoint < point)
        {
            bestPoint = point;
            bestPointText.text = ("Best score:" + bestPoint.ToString());
            congratulations.SetActive(true);
        }
        resultObj.SetActive(true);
        moveSpead = 14;
        StopAllCoroutines();
        timeText.text = ("Time in Play: " + TimeFormat.Format(timeInPlay));
        SaveManager.Instance.SaveGame();
    }
    public void MainBtn()
    {
        mainMenu.SetActive(true);
        canPlay = false;
    }
    private void Start()
    {
        AllStartCoroutine();
        StartCoroutine(TimePlay());
        SaveManager.Instance.LoadGame();
        canPlay = false;
    }
    public void StartGame()
    {
        mainMenu.SetActive(false);
        congratulations.SetActive(false);
        resultObj.SetActive(false);
        roadSpawner.StartGame();
        canPlay = true;
        StartCoroutine(TimePlay());
        AllStartCoroutine();
        moveSpead = 20;

        point = 0;
    }
    public void AddCoins(int c)
    {
        coins += c;
        RefreshTextCoins();
    }
    public void RefreshTextCoins()
    {
        coinsText.text = ("Coins:" + coins.ToString());
    }
    IEnumerator Point()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (boostEnable)
            {
                point += 2;

            }
            else
            {
                point++;
            }

            pointText.text = ("Score:" + point.ToString());
        }
    }
    public string AddPointToComet(int point)
    {
        this.point += point;
        return "Score:" + point.ToString();
    }
    IEnumerator TimePlay()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timeInPlay++;
        }
    }
    IEnumerator Spead()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.3f);
            moveSpead += 0.1f + Time.deltaTime;
            moveSpead = Mathf.Clamp(moveSpead, 14, 140);
        }
    }

    internal void RefreshText()
    {
        coinsText.text = ("Score: " + coins.ToString());
        bestPointText.text = ("Best score: " + bestPoint.ToString());
        countCometText.text = ("Count Comet: " + countComet.countComet.ToString());
    }

    public void AllStartCoroutine()
    {
        StartCoroutine(Point());
        StartCoroutine(Spead());
        camera.height = 5f;
    }

    public void boostOn()
    {
        if (!boostEnable)
        {
            moveSpead *= 2;
        }

        boostEnable = true;
        camera.height = 2f;
    }
    public void boostOff()
    {
        if (boostEnable)
        {
            moveSpead /= 2;
        }
        boostEnable = false;

        camera.height = 5f;

    }

    public void ActivateSkin(int skinIndex)
    {
        foreach (var skin in Skins)
        {
            skin.HideSkin();
        }
        Skins[skinIndex].ShowSkin();
    }

}
