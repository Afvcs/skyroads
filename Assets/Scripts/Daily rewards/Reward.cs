﻿[System.Serializable]
public class Reward 
{
   public enum RewardType
    {
        GOLD
    }
    public RewardType Type;
    public int Value;
    public string Name;
}
