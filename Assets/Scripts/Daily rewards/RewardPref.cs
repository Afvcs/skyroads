﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RewardPref : MonoBehaviour
{
    [SerializeField] private Image background;
    [SerializeField] private Color defaultColor;
    [SerializeField] private Color currentStreakColor;

    [Space(5)]
    [SerializeField] private Text dayText;
    [SerializeField] private Text rewardValue;

    [Space(5)]
    [SerializeField] private Image rewardIcon;
    [SerializeField] private Sprite rewardGold;
    public void SetRewardDate(int day,int currentStreak,Reward reward)
    {
        dayText.text = $"Day{day+1}";
        rewardIcon.sprite =  rewardGold ;
        rewardValue.text = reward.Value.ToString();

        background.color = day == currentStreak ? currentStreakColor : defaultColor;
    }
}
