﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyRewards : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;

    [Space(5)]
    [SerializeField] private Text status;
    [SerializeField] private Button claimBtn;

    [Space(5)]
    [SerializeField] private RewardPref rewardPref;
    [SerializeField] private Transform rewardsGrid;

    [Space(5)]
    [SerializeField] List<Reward> rewards;

    private int currentStreak
    {
        get => PlayerPrefs.GetInt("currentStreak", 0);
        set => PlayerPrefs.SetInt("currentStreak", value);
    }

    private DateTime? lastClaimTime
    {
        get
        {
            string data = PlayerPrefs.GetString("lastClaimTime", null);
            if (!string.IsNullOrEmpty(data))
            {
                return DateTime.Parse(data);
            }
            return null;
        }
        set
        {
            if (value != null)
            {
                PlayerPrefs.SetString("lastClaimTime", value.ToString());
            }
            else
            {
                PlayerPrefs.DeleteKey("lastClaimTime");
            }
        }
    }
    private List<RewardPref> rewardPrefabs;
    private bool canClaimReward;
    private int maxStreakCount = 7;
    private float claimCooldown = 24f;
    private float claimDeadLine = 48f;

    void Start()
    {


    }
    public void StartCoroutine()
    {
        InitPrefabs();
        StartCoroutine(RewardsStateUpdater());
    }




    private void InitPrefabs()
    {
        if (rewardPrefabs == null)
        {
            rewardPrefabs = new List<RewardPref>();

            for (int i = 0; i < maxStreakCount; i++)
            {
                rewardPrefabs.Add(Instantiate(rewardPref, rewardsGrid, false));
            }
        }



    }

    private IEnumerator RewardsStateUpdater()
    {
        while (true)
        {
            UpdateRewardsState();
            yield return new WaitForSeconds(1);
        }
    }

    private void UpdateRewardsState()
    {
        canClaimReward = true;
        if (lastClaimTime.HasValue)
        {
            var timeSpan = DateTime.UtcNow - lastClaimTime.Value;
            if (timeSpan.TotalHours > claimDeadLine)
            {
                lastClaimTime = null;
                currentStreak = 0;
            }
            else if (timeSpan.TotalHours < claimCooldown)
            {
                canClaimReward = false;
            }
        }
        UpdateRewardsUI();
    }
    private void UpdateRewardsUI()
    {
        claimBtn.interactable = canClaimReward;

        if (canClaimReward)
        {
            status.text = "claim your reward!";
        }
        else
        {
            var nextClaimTime = lastClaimTime.Value.AddHours(claimCooldown);
            var currentClaimCooldown = nextClaimTime - DateTime.UtcNow;
            string cd = $"{currentClaimCooldown.Hours:D2}:{currentClaimCooldown.Minutes:D2}:{currentClaimCooldown.Seconds:D2}";
            status.text = $"Come back in {cd} for your next reward";
        }
        for (int i = 0; i < maxStreakCount; i++)
        {
            rewardPrefabs[i].SetRewardDate(i, currentStreak, rewards[i]);
        }
    }
    public void claimReward()
    {
        if (!canClaimReward)
        {
            return;
        }
        var reward = rewards[currentStreak];
        switch (reward.Type)
        {
            case Reward.RewardType.GOLD:
                gameManager.AddCoins(reward.Value);
                break;
        }

        lastClaimTime = DateTime.UtcNow;
        currentStreak = (currentStreak + 1) % maxStreakCount;
        UpdateRewardsState();
    }

}
