﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    [SerializeField] private GameManager gameManager; 
    [SerializeField] private CountComet countComet;
    [SerializeField] private ShopManager shopManager;

    [SerializeField] private string filePath;

    public static SaveManager Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        gameManager = FindObjectOfType<GameManager>();   
        countComet = FindObjectOfType<CountComet>();
        filePath = Application.persistentDataPath + "data.gamesave";

        LoadGame();
        SaveGame();
    }

    public void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = new FileStream(filePath, FileMode.Create);

        Save save = new Save();
        save.activeIndexSkin = (int)shopManager.activeSkin;
        save.SaveBoughtItems(shopManager.shopItems);
        save.coins = gameManager.coins;
        save.bestPoint = gameManager.bestPoint;
        save.timeInPlay = gameManager.timeInPlay;
        save.countComet = countComet.countComet;
        bf.Serialize(fs, save);
        fs.Close();
    }

    public void LoadGame()
    {
        if (!File.Exists(filePath))
        {
            return;
        }


        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = new FileStream(filePath, FileMode.Open);

        Save save = (Save)bf.Deserialize(fs);


        gameManager.coins = save.coins;
        gameManager.bestPoint = save.bestPoint;
        gameManager.timeInPlay = save.timeInPlay;
        countComet.countComet = save.countComet;
        shopManager.activeSkin = (ShopItem.ItemType)save.activeIndexSkin;
        for (int i = 0; i < save.BoughtItems.Count; i++)
        {
            shopManager.shopItems[i].isBought = save.BoughtItems[i];
        }
        gameManager.ActivateSkin((int)shopManager.activeSkin);

        fs.Close();

        gameManager.RefreshText();
    }

}

[System.Serializable]
public class Save
{
    public int coins;
    public int bestPoint;
    public float timeInPlay;
    public int countComet;
    public int activeIndexSkin;
    public List<bool> BoughtItems = new List<bool>();
    public void SaveBoughtItems(List<ShopItem> items)
    {
        foreach (var item in items)
        {
            BoughtItems.Add(item.isBought);
        }
    }

}