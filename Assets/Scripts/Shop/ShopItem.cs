﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    public enum ItemType
    {
        FIRST_SKIN,
        SECOND_SKIN,
        THIRD_SKIN,
        FOURTH_SKIN,
        FIFTH_SKIN
    }
    public ItemType Type;
    [SerializeField] private Button buyBtn, activateBtn;
    public bool isBought;
    [SerializeField] private int cost;

    [SerializeField] GameManager gameManager;
    public ShopManager shopManager;

    bool IsActive
    {
        get
        {
            return Type == shopManager.activeSkin;
        }
    }

    public void Init()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    public void CheckButtons()
    {
        buyBtn.gameObject.SetActive(!isBought);
        buyBtn.interactable = CanBuy();

        activateBtn.gameObject.SetActive(isBought);
        activateBtn.interactable = !IsActive;
    }
    bool CanBuy()
    {
        return gameManager.coins >= cost;
    }

    public void BuyItems()
    {
        if (!CanBuy())
        {
            return;
        }

        isBought = true;
        gameManager.coins -= cost;

        CheckButtons();
        gameManager.RefreshTextCoins();

    }
    public void ActivateItem()
    {
        shopManager.activeSkin = Type;
        shopManager.CheckItemButtons();
        switch (Type)
        {
            case ItemType.FIRST_SKIN:
                gameManager.ActivateSkin(0);
                break;
            case ItemType.SECOND_SKIN:
                gameManager.ActivateSkin(1);
                break;
            case ItemType.THIRD_SKIN:
                gameManager.ActivateSkin(2);
                break;
            case ItemType.FOURTH_SKIN:
                gameManager.ActivateSkin(3);
                break;
            case ItemType.FIFTH_SKIN:
                gameManager.ActivateSkin(4);
                break;
            default:
                break;
        }
        SaveManager.Instance.SaveGame();
    }
}
