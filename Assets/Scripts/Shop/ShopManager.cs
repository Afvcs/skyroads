﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopManager : MonoBehaviour
{
    public List<ShopItem> shopItems;
    public ShopItem.ItemType activeSkin;
    public void OpenShop()
    {
        CheckItemButtons();
        gameObject.SetActive(true);
    }
    public void CloseShop()
    {
        gameObject.SetActive(false);
    }

    public void CheckItemButtons()
    {
        foreach (ShopItem item in shopItems)
        {
            item.shopManager = this;
            item.Init();
            item.CheckButtons();
        }
    }
}
