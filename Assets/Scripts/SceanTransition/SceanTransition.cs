﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceanTransition : MonoBehaviour
{
    private Animator componentAnimator;

    private static SceanTransition instance;
    private static bool shouldPlayOpeningAnimation = false;

    private AsyncOperation asyncOperation;

    public static void SwitchToScene(string sceneName)
    {
        instance.componentAnimator.SetTrigger("SceanClosing");
        instance.asyncOperation = SceneManager.LoadSceneAsync(sceneName);
        instance.asyncOperation.allowSceneActivation = false;
    }
    void Start()
    {
        instance = this;
        componentAnimator = GetComponent<Animator>();
        if (shouldPlayOpeningAnimation)
        {
            componentAnimator.SetTrigger("SceanOpening");
        }
    }
    public void OnAnimationOver()
    {
        shouldPlayOpeningAnimation = true;
        asyncOperation.allowSceneActivation = true;
    }
}
