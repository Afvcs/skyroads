﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public AudioSource BGAS, EffectCoinsAS, EffectBoomAS;
    [SerializeField] private AudioMixerGroup master, music, effect;
    [SerializeField] private Toggle toggle;
    [SerializeField] private Slider sliderMusic, sliderEffect;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }
    public void Start()
    {
        master.audioMixer.SetFloat("Master", valueMaster);
        music.audioMixer.SetFloat("music", valueMusic);
        effect.audioMixer.SetFloat("effect", valueEffect);
    }
    public void openSettings()
    {
        sliderMusic.value = valueMusic;
        sliderEffect.value = valueEffect;
        toggle.isOn = valueMaster == 0 ? true : false;
    }
    private float valueMusic
    {
        get => PlayerPrefs.GetFloat("music", 0);
        set => PlayerPrefs.SetFloat("music", value);
    }
    private float valueEffect
    {
        get => PlayerPrefs.GetFloat("effect", 0);
        set => PlayerPrefs.SetFloat("effect", value);
    }
    private float valueMaster
    {
        get => PlayerPrefs.GetFloat("master", 0);
        set => PlayerPrefs.SetFloat("master", value);
    }

    public void ToggleMusic()
    {
        if (toggle.isOn)
        {
            master.audioMixer.SetFloat("Master", 0);
            valueMaster = 0;
            sliderMusic.value = 0;
            sliderEffect.value = 0;
            Debug.Log("valueMusic" + valueMusic); Debug.Log(" valueEffect" + valueEffect);
        }
        else
        {
            master.audioMixer.SetFloat("Master", -80);
            valueMaster = -80;
            sliderMusic.value = -30;
            sliderEffect.value = -30;
        }
    }
    public void ChangeVolumeMusic()
    {
        
            valueMusic = sliderMusic.value;
          
        
        music.audioMixer.SetFloat("music", sliderMusic.value);

        if (!toggle.isOn && valueMusic > -30)
        {
            toggle.isOn = true;
        }
        if (toggle.isOn && valueEffect == -30 && valueMusic == -30)
        {
            toggle.isOn = false;
        }
    }
    public void ChangeVolumeEffect()
    {
        
            valueEffect = sliderEffect.value;
           
        
        effect.audioMixer.SetFloat("effect", sliderEffect.value);

        if (toggle.isOn && valueEffect == -30 && valueMusic == -30)
        {
            toggle.isOn = false;
        }
        if (!toggle.isOn && valueEffect > -30)
        {
            toggle.isOn = true;
        }
    }

    public void PlayCoinEffect()
    {
        EffectCoinsAS.Play();
    }
    public void PlayBoomEffect()
    {

        EffectBoomAS.Play();
    }
}